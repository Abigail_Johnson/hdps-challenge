import re
import time
#from random import randint
import requests
import os
import json

START_SPELL_CHECK="<span class=\"spell\">Showing results for</span>"
END_SPELL_CHECK="<br><span class=\"spell_orig\">Search instead for"

HTML_Codes = (
    ("'", '&#39;'),
    ('"', '&quot;'),
    ('>', '&gt;'),
    ('<', '&lt;'),
    ('&', '&amp;'),
)

class SpellChecker(object):
    CACHE_FILENAME = "spell_check_cache.json"

    def __init__(self, refresh_cache=False):
        self.refresh_cache = refresh_cache
        self.cache = self.load_cache()

    def load_cache(self):
        cache = {}
        if self.refresh_cache:
            return cache

        if os.path.isfile(SpellChecker.CACHE_FILENAME):
            with open(SpellChecker.CACHE_FILENAME) as data_file:
                cache = json.load(data_file)

        return cache

    def save_cache(self):
        with open(SpellChecker.CACHE_FILENAME, 'w') as out_file:
            json.dump(self.cache, out_file, indent=2)

    def spell_check(self, s):
        # Check the cache
        if s in self.cache:
            return self.cache[s]

        q = '+'.join(s.split())
        #time.sleep(  randint(0,2) ) #relax and don't let google be angry
        time.sleep(0.05)
        r = requests.get("http://www.google.com/search?q="+q)
        content = r.text
        start=content.find(START_SPELL_CHECK)
        if start > -1:
            start = start + len(START_SPELL_CHECK)
            end=content.find(END_SPELL_CHECK)
            search = content[start:end]
            search = re.sub(r'<[^>]+>', '', search)
            for code in HTML_Codes:
                search = search.replace(code[1], code[0])
            search = search[1:]
        else:
            search = s

        self.cache[s] = search
        self.save_cache()
        return search

if __name__ == "__main__":
    searches = ["metal plate cover gcfi", "artric air portable",
                "roll roofing lap cemet", "basemetnt window",
                "vynal grip strip", "lawn mower- electic" ]

    checker = SpellChecker()

    for sentence in searches:
        correct_sentence = checker.spell_check(sentence)
        print sentence, "->", correct_sentence
