#!/usr/bin/env python
import pandas as pd
import logging
import math

from spell_check import SpellChecker

logger = logging.getLogger("Spell Checker")
#MAX_TRAIN = 100
MAX_TRAIN = None

def my_round(x, base=5):
    return int(base * math.floor(float(x)/base))

def refresh_spelling_cache(column):
    """ Fixes the spelling of all strings in a pandas Series.

    Args:
        column (pd.Series): A pandas series with strings that have spelling errors.

    Returns:
        A pandas Series with the corrected spellings.
    """
    # WARNING: This code is dangerous and will destroy the previous cache without
    # a backup. Use at your own risk.
    #checker = SpellChecker(refresh_cache=True)
    checker = SpellChecker(refresh_cache=False)
    size = len(column)
    last_percent = 0

    corrected_strings = pd.Series()
    for i, string in enumerate(column):
        percent = my_round((float(i) / size) * 100, 5)
        if percent > last_percent:
            print "Finished", str(percent) + "%"
            last_percent = percent
        correct = checker.spell_check(string)
        logger.debug(string + "->" + correct)
        corrected_strings.set_value(i, correct)

    print "Finished 100%"
    return corrected_strings

if __name__ == "__main__":
    train = pd.read_csv("train.csv", encoding="ISO-8859-1")
    if MAX_TRAIN is not None:
        train = train[:MAX_TRAIN]

    refresh_spelling_cache(train['search_term'])
